package com.central.cms.service.impl;

import com.central.cms.service.CpCmsMenuService;
import com.central.cms.commons.base.service.impl.BaseServiceImpl;
import com.central.cms.mybatis.model.CpCmsMenu;
import com.central.cms.mybatis.mapper.CpCmsMenuMapper;
import org.springframework.stereotype.Service;

@Service
public class CpCmsMenuServiceImpl extends BaseServiceImpl<CpCmsMenuMapper, CpCmsMenu> implements CpCmsMenuService {


}
