package com.central.cms.service.impl;

import com.central.cms.service.CpCmsModelFiledService;
import com.central.cms.commons.base.service.impl.BaseServiceImpl;
import com.central.cms.mybatis.model.CpCmsModelFiled;
import com.central.cms.mybatis.mapper.CpCmsModelFiledMapper;
import org.springframework.stereotype.Service;

@Service
public class CpCmsModelFiledServiceImpl extends BaseServiceImpl<CpCmsModelFiledMapper, CpCmsModelFiled> implements CpCmsModelFiledService {


}
