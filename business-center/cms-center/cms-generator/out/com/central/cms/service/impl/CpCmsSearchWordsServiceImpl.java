package com.central.cms.service.impl;

import com.central.cms.service.CpCmsSearchWordsService;
import com.central.cms.commons.base.service.impl.BaseServiceImpl;
import com.central.cms.mybatis.model.CpCmsSearchWords;
import com.central.cms.mybatis.mapper.CpCmsSearchWordsMapper;
import org.springframework.stereotype.Service;

@Service
public class CpCmsSearchWordsServiceImpl extends BaseServiceImpl<CpCmsSearchWordsMapper, CpCmsSearchWords> implements CpCmsSearchWordsService {


}
