package com.central.cms.service.impl;

import com.central.cms.service.CpCmsMemberService;
import com.central.cms.commons.base.service.impl.BaseServiceImpl;
import com.central.cms.mybatis.model.CpCmsMember;
import com.central.cms.mybatis.mapper.CpCmsMemberMapper;
import org.springframework.stereotype.Service;

@Service
public class CpCmsMemberServiceImpl extends BaseServiceImpl<CpCmsMemberMapper, CpCmsMember> implements CpCmsMemberService {


}
