package com.central.cms.service.impl;

import com.central.cms.service.CpCmsAdminSiteService;
import com.central.cms.commons.base.service.impl.BaseServiceImpl;
import com.central.cms.mybatis.model.CpCmsAdminSite;
import com.central.cms.mybatis.mapper.CpCmsAdminSiteMapper;
import org.springframework.stereotype.Service;

@Service
public class CpCmsAdminSiteServiceImpl extends BaseServiceImpl<CpCmsAdminSiteMapper, CpCmsAdminSite> implements CpCmsAdminSiteService {


}
