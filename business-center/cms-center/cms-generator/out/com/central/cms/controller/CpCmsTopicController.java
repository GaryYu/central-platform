package com.central.cms.controller;

import com.central.cms.mybatis.model.CpCmsTopic;
import com.central.cms.service.CpCmsTopicService;
import com.github.pagehelper.PageInfo;
import com.central.cms.commons.base.controller.BaseController;
import com.central.cms.commons.base.controller.response.Result;
import com.central.cms.commons.base.controller.response.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/")
public class CpCmsTopicController extends BaseController {

    @Autowired
    private CpCmsTopicService cpcmstopicService;


    @GetMapping("find/{id}")
    public Result find(@PathVariable("id") Object Id){
        CpCmsTopic cpcmstopic = cpcmstopicService.findById(Id);
        return ResultGenerator.genSuccessResult().setData(cpcmstopic);
    }

    @PostMapping("delete/{id}")
    public Result save(@PathVariable("id") Object Id){
        cpcmstopicService.deleteById(Id);
        return ResultGenerator.genSuccessResult().setInfo("删除成功！");

    }

    @PostMapping("save")
    public Result save(CpCmsTopic cpcmstopic){
        cpcmstopicService.insertSelective(cpcmstopic);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @PostMapping("update")
    public Result update(CpCmsTopic cpcmstopic){
        cpcmstopicService.updateSelectiveById(cpcmstopic);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @GetMapping("page")
    public Result page(@RequestParam(value = "pageNumber",defaultValue = "1") Integer pageNumber,
                        @RequestParam(value = "pageSize",defaultValue = "30") Integer pageSize,
                        CpCmsTopic queryBean){
        PageInfo<CpCmsTopic> page = cpcmstopicService.page(pageNumber,pageSize,queryBean);
        return ResultGenerator.genSuccessResult().setData(page.getList());

    }




}
