package com.central.cms.controller;

import com.central.cms.mybatis.model.CpCmsContent;
import com.central.cms.service.CpCmsContentService;
import com.github.pagehelper.PageInfo;
import com.central.cms.commons.base.controller.BaseController;
import com.central.cms.commons.base.controller.response.Result;
import com.central.cms.commons.base.controller.response.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/")
public class CpCmsContentController extends BaseController {

    @Autowired
    private CpCmsContentService cpcmscontentService;


    @GetMapping("find/{id}")
    public Result find(@PathVariable("id") Object Id){
        CpCmsContent cpcmscontent = cpcmscontentService.findById(Id);
        return ResultGenerator.genSuccessResult().setData(cpcmscontent);
    }

    @PostMapping("delete/{id}")
    public Result save(@PathVariable("id") Object Id){
        cpcmscontentService.deleteById(Id);
        return ResultGenerator.genSuccessResult().setInfo("删除成功！");

    }

    @PostMapping("save")
    public Result save(CpCmsContent cpcmscontent){
        cpcmscontentService.insertSelective(cpcmscontent);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @PostMapping("update")
    public Result update(CpCmsContent cpcmscontent){
        cpcmscontentService.updateSelectiveById(cpcmscontent);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @GetMapping("page")
    public Result page(@RequestParam(value = "pageNumber",defaultValue = "1") Integer pageNumber,
                        @RequestParam(value = "pageSize",defaultValue = "30") Integer pageSize,
                        CpCmsContent queryBean){
        PageInfo<CpCmsContent> page = cpcmscontentService.page(pageNumber,pageSize,queryBean);
        return ResultGenerator.genSuccessResult().setData(page.getList());

    }




}
